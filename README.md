# SCGI library for Vala, or libscgi

This library let you use the [SimpleCG protocol](http://www.python.ca/scgi/protocol.txt) in Vala to write an app that can communicate with a web server like Nginx.

This version is based on the [AmbitionFramework's one](https://github.com/AmbitionFramework/libgscgi), itself based on [madmo's version](https://bitbucket.org/madmo/libgscgi/src)

The changes are that we renamed the namespace to `SCGI` instead of `scgi`, removed debian files and used Gee instead of GLib's generics.

## Little Documentation

This library consists in only two classes.

### Server

This class represents a SCGI server, that can handle request sent by Nginx or another
web server.

You can create a new server like this.

```vala
new Server (port, max_threads, handler);
```

Where :

- `port` is the port on which to listen. Remember to configure your web server correctly, to listen on the same port as your app.
- `max_threads` the maximum number of threads that can be runned at the same time.
- `handler` is a delegate `void`, that takes a `SCGI.Request` as argument. It will be executed for each incoming request.

### Request

This is the representation of a SimpleCGI request. The `handler` parameter of the `Server` constructor
takes one as parameter.

It as the following properties :

- `params`, a [`Gee.HashMap<string, string>`](http://valadoc.org/#!api=gee-0.8/Gee.HashMap),
containing the parameters sent by the web server as key and their values as values (logical, isn't it). For example, if
you use Nginx, you'll have a parameter named `REQUEST_METHOD` containing the HTTP method of the request
(GET, POST, PUT, DELETE ...).
- `input`, which is an [`InputStream`](http://valadoc.org/gio-2.0/GLib.InputStream.html) that can be casted into a
[`DataInputStream`](http://valadoc.org/gio-2.0/GLib.DataInputStream.html). You can read it to  get
the request body, for example in a POST request.
- `output`, an [`OutputStream`](http://valadoc.org/gio-2.0/GLib.OutputStream.html) that can be casted into a
[`DataOutputStream`](http://valadoc.org/gio-2.0/GLib.DataOutputStream.html). You use it to write your response, following
the [HTTP protocol](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol).

## Example

You'll need to install the lib first, by cloning this project and running `make install` if you are on Linux, else if you
 are on Windows, do `make all`, and copy output files to your project's folder.

Then create a file, `test.vala`.

```vala
using SCGI;

void main () {
    MainLoop ml = new MainLoop ();
    new Server (9000, 25, (res) => {
        // try/catch is important, if you don't write it, it'll not work
        try {
            print ("Someone wants the page at %s.\n", res.params["DOCUMENT_URI"]);
            print ("Request body (for POST and Co):\n");
            DataInputStream dis = (DataInputStream) res.input;
            string line;
            int i = 0;
            while ((line = dis.read_line (null)) != null) {
                if (line == "") {
                    break;
                }
                print ("%s\n", line);
                i++;
                if (i == 3) {
                    break;
                }
            }

            // Sending a response
            res.output.write ("Content-Type: text/html\r\n".data);
            res.output.write ("\r\n".data);
            res.output.write ("<h1>Hello world</h1>\r\n".data);
        } catch (Error err) {
            print ("[ERROR] %s\n", err.message);
        }

    });
    ml.run();
}
```

If you're on Linux you can compile with this command.

```bash
valac test.vala --pkg libscgi-1.0
```

On Windows, you can do this.

```powershell
valac --vapidir=. --pkg libgscgi-1.0 -X "-I." -X libgscgi-1.0.dll --pkg libsoup-2.4 --pkg gee-1.0 --pkg gee-1.0 test.vala
```

Then, run Nginx or antoher web server and make it listen for SimpleCGI on port 9000.
 Just go on [http://localhost](http://localhost) and let the magic happens !
