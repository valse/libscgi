PREFIX ?= /usr
LIBDIR ?=
ifeq ($(LIBDIR),)
ifeq ($(HOST_ARCH), x86_64)
	LIBDIR = lib64
else
	LIBDIR = lib
endif
endif
INSTALLDIR = $(DESTDIR)$(PREFIX)

OUT = libscgi-1.0

ifeq ($(OS),Windows_NT)
    LIBRARY = $(OUT).dll
else
    LIBRARY = $(OUT).so
endif

SRC = Request.vala Server.vala
PKGS = --pkg gio-2.0 --pkg gee-0.8

VALAC = valac
VALACOPTS = -g --library $(OUT) -X -shared -X -fPIC -H $(OUT).h

all: $(LIBRARY)

$(LIBRARY):
	@echo "VALAC"
	@$(VALAC) $(VALACOPTS) $(SRC) -o $(LIBRARY) $(PKGS)

# Not working on Windows
install: $(LIBRARY)
	@echo "Installing"
	@echo $(INSTALLDIR)/$(LIBDIR)/
	@mkdir -p $(INSTALLDIR)/share/vala/vapi
	@mkdir -p $(INSTALLDIR)/share/pkgconfig
	@mkdir -p $(INSTALLDIR)/$(LIBDIR)
	@install -m 755 -d $(INSTALLDIR)/lib/pkgconfig/ $(INSTALLDIR)/include/ $(INSTALLDIR)/share/vala/vapi
	@install -m 755 $(LIBRARY) $(INSTALLDIR)/$(LIBDIR)/
	@install -m 644 $(OUT).h $(INSTALLDIR)/include/
	@install -m 644 $(OUT).vapi $(INSTALLDIR)/share/vala/vapi
	@install -m 644 $(OUT).deps $(INSTALLDIR)/share/vala/vapi
	@sed -e 's/@LIBDIR@/$(subst /,\/,$(INSTALLDIR))\/$(LIB_DIR)/' -e 's/@INCLUDEDIR@/$(subst /,\/,$(INSTALLDIR))\/include/' $(OUT).pc.in > $(INSTALLDIR)/share/pkgconfig/$(OUT).pc

clean:
	@echo "Cleaning"
	@rm -v -fr *~ *.c $(LIBRARY) *.vapi *.h

.PHONY: clean all install
