Summary:            A library to use SimpleCGI in Vala and GObject
Name:               libscgi
Version:            1.0
Release:            1%{?dist}
License:            LGPLv3
Group:              System Development/Libraries
URL:                https://framagit.org/valse/libscgi
ExcludeArch:        s390 s390x
BuildRequires:      vala, libgee-devel

%description
This library makes it easy to use the SimpleCGI protocol in Vala. This protocol
is used to commucate between a web server and an application.

%build
make %{?_smp_mflags} PREFIX=%{_prefix} LIBDIR=lib64

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} LIBDIR=lib64

%files
%doc README.md
%license LICENSE
%{_libdir}/libscgi-1.0.so
%{_includedir}/libscgi-1.0.h
%{_datadir}/vala/vapi/libscgi-1.0.vapi
%{_datadir}/vala/vapi/libscgi-1.0.deps
%{_datadir}/pkgconfig/libscgi-1.0.pc
